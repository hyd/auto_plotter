# Auto Plotter

## Description
Auto Plotter is a tool that automates the plotting of JV characteristics and provides box plots for Voc, Jsc, FF, and PCE for each file corresponding to a sample. Additionally, it generates a summary Excel file with all available raw data.

## Installation
To use Auto Plotter, you can download the executable file and place it in the folder containing your raw data. The program will automatically process the data and generate the plots and summary file.

Alternatively, you can run the program from the source code. Ensure that all necessary libraries are installed by using the `requirements.txt` file. Update the folder path in the code to point to your data directory.

### Steps:
1. **Executable File**: Download the Auto Plotter executable file and place it in the folder with your raw data.
2. **Source Code**:
   - Clone the repository
   - Install the necessary libraries:
     ```
     pip install -r requirements.txt
     ```
   - Update the folder path in the code to your data directory.
   - Run the program:
     ```
     python auto_plotter.py
     ```

## Usage
Place the executable file in the folder with the raw data and run it. The program will generate JV characteristic plots, box plots for Voc, Jsc, FF, and PCE, and a summary Excel file with all raw data.

Example:
1. Place `auto_plotter.exe` in your data folder.
2. Run `auto_plotter.exe`.
3. Find the generated plots and summary Excel file in the same folder.

## Support
For support, you can reach out via the issue tracker on the GitLab repository or contact via email: [support@example.com].

## Roadmap
Future plans for Auto Plotter include:
- Adding support for additional plot types
- Enhancing the summary report with more detailed analytics
- Improving the user interface for easier configuration

## Contributing
We welcome contributions! If you'd like to contribute, please fork the repository and create a pull request. Ensure that you follow the contribution guidelines provided in the repository.

To get started:
1. Fork the repository
2. Create a new branch (`git checkout -b feature-branch`)
3. Make your changes
4. Commit your changes (`git commit -am 'Add new feature'`)
5. Push to the branch (`git push origin feature-branch`)
6. Create a new Pull Request

## Authors and acknowledgment
Developed by Edgar Nandayapa. Special thanks to all contributors and supporters.

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Project status
Active - The project is currently being actively developed and maintained.