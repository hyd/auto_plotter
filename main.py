__author__ = "Edgar Nandayapa"
__version__ = "v0.0.1 2024"

from glob import glob
from itertools import cycle
import pandas as pd
import seaborn as sns
import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import openpyxl
from matplotlib.ticker import MaxNLocator


def load_files(folder_b):
    # Gather file list
    file_list = glob(folder_b + "*.txt")

    jv_files = []
    mpp_files = []
    if len(file_list) != 0:
        for fi in file_list:
            if "mpp" not in fi:
                jv_files.append(fi)
            else:
                mpp_files.append(fi)

    # Collect relevant data
    jvc_merged = pd.DataFrame()
    cur_merged = pd.DataFrame()
    for cc, f in enumerate(jv_files):
        linepos = find_separators_in_file(f)

        # JV Characteristics
        jvc_data = pd.read_csv(f, skiprows=linepos[0] + 1, header=None, index_col=0, nrows=9, delimiter="\t",
                               encoding='ISO-8859-1').T

        jvc_data["subfolder"] = f.split("\\")[-2]
        jvc_data["producer"] = f.split("\\")[-1].split("-")[0]
        jvc_data["processing"] = f.split("\\")[-1].split("-")[1]
        jvc_data["wafer"] = f.split("\\")[-1].split("-")[2]
        jvc_data["sample"] = f.split("\\")[-1].split("-")[3]
        jvc_data["measurement"] = f.split("\\")[-1].split("-")[4].split(".")[0]
        jvc_data["SampleName"] = f.split("\\")[-1].split(".")[0]

        # JV curve
        curve_data = pd.read_csv(f, skiprows=linepos[1], header=0, index_col=None, delimiter="\t",
                                 encoding='ISO-8859-1').T
        curve_data = curve_data.drop(curve_data.columns[0], axis=1)
        curve_data = curve_data.apply(pd.to_numeric, errors='coerce')

        for indx in curve_data.index:
            sample_name = f.split("\\")[-1].split(".")[0]
            sample_name = f"{indx}_{sample_name}"
            curve_data.rename(index={indx: sample_name}, inplace=True)

        # Gather data
        if cc == 0:
            jvc_merged = jvc_data.copy()
            cur_merged = curve_data.copy()
        else:
            jvc_merged = pd.concat([jvc_merged, jvc_data])
            cur_merged = pd.concat([cur_merged, curve_data.iloc[1:]])

    jvc_merged = jvc_merged.reset_index(drop=True)
    cur_merged = cur_merged

    jvc_merged = jvc_merged.rename(columns={'J_sc [mA/cm²]:': 'J_sc [mA/cm²]', 'V_oc [V]:': 'V_oc [V]',
                                            'Fill factor [pct.]:': 'Fill Factor [%]', 'Efficiency :': 'PCE [%]',
                                            'P_MPP [mW/cm²]:': 'P_MPP [mW/cm²]', 'J_MPP [mA/cm²]:': 'J_MPP [mA/cm²]',
                                            'U_MPP [V]:': 'U_MPP [V]', 'R_ser [Ohmcm²]:': 'R_ser [Ohmcm²]',
                                            'R_par [Ohmcm²]:': 'R_par [Ohmcm²]'})

    mpp_merged = read_mpp_files(mpp_files)

    return jvc_merged, cur_merged.T, mpp_merged.T


def read_mpp_files(files):
    # mpp tracking data
    mpp_merged = pd.DataFrame()
    for mm, f in enumerate(files):
        # Identify rows to skip
        rows_to_skip = []
        with open(f, 'r', encoding='ISO-8859-1') as file:
            for i, line in enumerate(file):
                if "average power (last 50% of values):" in line:
                    rows_to_skip.append(i)

        linepos = find_separators_in_file(f)

        skip_list = list(range(linepos[0])) + rows_to_skip

        mpp_data = pd.read_csv(f, skiprows=skip_list, header=0, index_col=None, delimiter="\t",
                               encoding='ISO-8859-1').T

        mpp_data = mpp_data.apply(pd.to_numeric, errors='coerce')
        for indx in mpp_data.index:
            sample_namem = f.split("\\")[-1].split(".")[0].rsplit('-', 1)[0]
            sample_namem = f"{indx}_{sample_namem}"
            mpp_data.rename(index={indx: sample_namem}, inplace=True)

        if mm == 0:
            mpp_merged = mpp_data.copy()
        else:
            mpp_merged = pd.concat([mpp_merged, mpp_data])
            mpp_merged = keep_row_with_least_nans(mpp_merged)

    return mpp_merged


def keep_row_with_least_nans(df):
    # Filter rows with 'time' in the index
    time_rows = df[df.index.str.contains('time')]

    # Identify the row with the least NaNs
    row_least_nans = time_rows.isna().sum(axis=1).idxmin()

    # Reorder the DataFrame to make this row the first row
    df = pd.concat([df.loc[[row_least_nans]], df.drop(row_least_nans)])

    # Remove all but the first 'time' row
    first_time_row = df.index[df.index.str.contains('time')][0]
    df = df.drop(index=df.index[df.index.str.contains('time') & (df.index != first_time_row)])

    return df


def find_separators_in_file(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            lines = file.readlines()
    except UnicodeDecodeError:
        with open(file_path, 'r', encoding='ISO-8859-1') as file:
            lines = file.readlines()

    positions = []
    for index, line in enumerate(lines):
        if '*****' in line.strip():
            positions.append(index + 1)

    return positions


def plot_4x5_samples(df, sample, path):
    # print(df.columns)
    # filtered_columns = [col for col in df.columns if col.split('-')[-3] == str(sample)]

    # Pair 'j_forw' and 'j_rev' columns
    paired_columns = []
    for col in df:
        if 'j_forw' in col:
            rev_col = col.replace('j_forw', 'j_rev')
            if rev_col in df:
                paired_columns.append((col, rev_col))

    # Set up the matplotlib figure and axes
    fig, axs = plt.subplots(4, 5, figsize=(20, 16))  # Adjust figsize as needed
    axs = axs.flatten()  # Flatten the 2D array of axes for easy iteration

    # Iterate over the paired columns and plot each pair
    for i, (forw_col, rev_col) in enumerate(paired_columns):
        if i < 20:  # Make sure we don't exceed 20 plots
            axs[i].plot(df.iloc[:, 0], df[forw_col], label='j_forw')
            axs[i].plot(df.iloc[:, 0], df[rev_col], label='j_rev')
            axs[i].set_title(forw_col.split('_', 2)[2])  # Set title to the sample code

            # Set the number of ticks on each axis
            axs[i].xaxis.set_major_locator(MaxNLocator(nbins=5))  # Adjust 'nbins' as needed
            axs[i].yaxis.set_major_locator(MaxNLocator(nbins=5))  # Adjust 'nbins' as needed

            # Adding a horizontal line at y=0
            axs[i].axhline(y=0, color='gray')
            # Adding a vertical line at x=0
            axs[i].axvline(x=0, color='gray')

            # Setting y-axis range from -5 to 20 and grid
            axs[i].set_ylim(-5, 20)
            axs[i].grid(True, linestyle='--')

            axs[i].legend()

    # Adjust layout
    plt.tight_layout()
    # plt.show()
    fig.savefig(path+f"0_jv-separated_{sample}.png", dpi=300)
    plt.close()
    print(f"Saved 0_jv-separated_{sample}.png")


def plot_curves_together(df1, df2, path, namestring="All"):
    # Plot all curves in dim gray
    for column in df2.columns:
        line_style = 'dashed' if 'rev' in column else 'solid'
        plt.plot(df2.iloc[:, 0], df2[column], color='dimgray', linestyle=line_style, linewidth=0.5)

    # Sort df1 by 'PCE [%]' in descending order
    sorted_df1 = df1.sort_values('PCE [%]', ascending=False)

    # Select the top 3 samples based on 'PCE [%]'
    top_samples = sorted_df1.head(4)['SampleName'].values

    style = ["--", "-.", ":", "-"]
    # Plot the curves for these samples
    for count, sample in enumerate(top_samples):
        plt.plot(df2.iloc[:, 0], df2[df2.filter(like=sample).columns[0]],
                 label=f'{sample}', linewidth=2, linestyle=style[count])

    plt.plot([], [], label='Forward', color='dimgray', linestyle="solid", linewidth=0.5)
    plt.plot([], [], label='Reverse', color='dimgray', linestyle="dashed", linewidth=0.5)

    # Adding a horizontal line at y=0
    plt.axhline(y=0, color='gray')

    # Adding a vertical line at x=0
    plt.axvline(x=0, color='gray')

    # Setting y-axis range from -5 to 20 and grid
    plt.ylim(-5, 20)

    # Adding labels and legend
    plt.xlabel('Voltage (V)')  # Replace with your actual x-axis label
    plt.ylabel('Current density (mA/cm²)')  # Replace with your actual y-axis label
    plt.ylim(-5, 20)
    plt.title(f'{namestring} samples')
    plt.legend()

    # Show the plot
    plt.savefig(path+f"0_jv-together_{namestring}.png", dpi=300)
    plt.close()
    print(f"Saved 0_jv-together_{namestring}.png")


def plot_all_max_pce_together(df1, df2, path):
    df1_reset = df1.reset_index()
    idx = df1_reset.groupby('subfolder')['PCE [%]'].idxmax()

    # Use the index to get the rows with the highest 'PCE [%]' in each subfolder
    highest_pce_per_subfolder = df1_reset.loc[idx]

    style = cycle(["--", "-.", ":", "-"])
    count = 0
    # Plot the curves for the samples with the highest PCE in each subfolder
    for _, row in highest_pce_per_subfolder.iterrows():
        sample_name = row['SampleName']
        subfolder = row['subfolder']
        # Assuming df2 has a column for each 'SampleName' with the related data to plot
        plt.plot(df2.iloc[:, 0], df2[df2.filter(like=sample_name).columns[0]],
                 label=f'{subfolder} - ({sample_name})', linewidth=2, linestyle=next(style))
        # print(df2[df2.filter(like=sample_name).columns[0]])
        count += 1

    # Adding a horizontal line at y=0
    plt.axhline(y=0, color='gray')

    # Adding a vertical line at x=0
    plt.axvline(x=0, color='gray')

    # Setting y-axis range from -5 to 20 and grid
    plt.ylim(-5, 20)

    # Set the labels and title
    plt.xlabel('Voltage (V)')  # Replace with the actual x-axis label
    plt.ylabel('Current (mA/cm²)')  # Replace with the actual y-axis label
    plt.title('Highest PCE Curves by Group')
    plt.legend()

    # Show the plot
    plt.savefig(path + f"0_jv-max_by_group.png", dpi=300)
    plt.close()
    print(f"Saved 0_jv-max_by_group.png")


def plot_mpp(df, path, stringname="all"):
    for column in df.keys():
        if "power" in column:
            df[column] = df[column].abs()
            sample_name = column.split("_")[1]
            plt.plot(df.iloc[:, 0], df[column], label=sample_name)

    # Adding labels and legend
    plt.xlabel('Time (s)')  # Replace with your actual x-axis label
    plt.ylabel('Power (mW/cm²) ')  # Replace with your actual y-axis label
    plt.title('MPP tracking')
    plt.legend()
    plt.grid(True, linestyle='--')

    # Show the plot
    # plt.show()
    plt.savefig(path + f"0_mpp-tracking_{stringname}.png", dpi=300)
    plt.close()
    print(f"Saved 0_mpp-tracking_{stringname}.png")


def read_subfolder_data(path):
    subfolder = glob(path + "*\\")

    full_data = pd.DataFrame()
    for cc, sf in enumerate(subfolder):
        data, _, _ = load_files(sf)
        data["condition"] = sf.split("\\")[-2]

        if cc == 0:
            full_data = data.copy()
        else:
            full_data = pd.concat([full_data, data])

    return full_data


def box_plot_making_main(folder_b):
    df = read_subfolder_data(folder_b)

    fig, axs = plt.subplots(2, 2, figsize=(15, 10))  # Adjust figsize as needed
    axs = axs.flatten()  # Flatten the 2D array of axes for easy iteration

    # Assuming boxplot_all_cells is a function that takes a DataFrame, condition, metric, and an axes object
    boxplot_all_cells(df, 'condition', 'J_sc [mA/cm²]', axs[0])
    axs[0].set_title("Current density")

    boxplot_all_cells(df, 'condition', 'V_oc [V]', axs[1])
    axs[1].set_title("Voltage")

    boxplot_all_cells(df, 'condition', 'Fill Factor [%]', axs[2])
    axs[2].set_title("Fill Factor")

    boxplot_all_cells(df, 'condition', 'PCE [%]', axs[3])
    axs[3].set_title("PCE")

    # plt.show()
    fig.savefig(folder_b + "Results\\0_jv-boxplots_main.png", dpi=300)
    plt.close()
    print("Saved 0_jv-boxplots_main.png")


def box_plot_making_resistance(folder_b):
    df = read_subfolder_data(folder_b)
    df = df.dropna()

    fig, axs = plt.subplots(1, 2, figsize=(15, 5))  # Adjust figsize as needed
    axs = axs.flatten()  # Flatten the 2D array of axes for easy iteration

    # Assuming boxplot_all_cells is a function that takes a DataFrame, condition, metric, and an axes object
    boxplot_all_cells(df, 'condition', 'R_ser [Ohmcm²]', axs[0])
    axs[0].set_title("Series Resistance")

    boxplot_all_cells(df, 'condition', 'R_par [Ohmcm²]', axs[1])
    axs[1].set_title("Shunt Resistance")

    # plt.show()
    fig.savefig(folder_b + "Results\\0_jv-boxplots_resistance.png", dpi=300)
    plt.close()
    print("Saved 0_jv-boxplots_resistance.png")


def boxplot_all_cells(df, var_x, pl_y, ax=None):
    # Plotting style and colors
    sns.set(style="whitegrid")
    ax.grid(True, linestyle='--')

    df["V_oc [V]"] = df["V_oc [V]"].abs()
    df["PCE [%]"] = df["PCE [%]"].abs()

    # Calculate statistics and add median with respect to var_x
    desc = df.groupby(var_x)[pl_y].describe()

    desc["median"] = df.groupby(var_x)[pl_y].median()

    order_parameter = "alphabetic"  # 'count', 'mean', 'std', 'min','max', 'median','alphabetic'
    if order_parameter != "alphabetic":
        orderc = desc.sort_values(by=[order_parameter])["count"].index
        nobs = desc.sort_values(by=[order_parameter])["count"].values
    else:
        orderc = desc.sort_index()["count"].index
        nobs = desc.sort_index()["count"].values

    # Plotting
    ax = sns.boxplot(x=var_x, y=pl_y, data=df, showfliers=False, showmeans=True,
                     order=orderc, meanprops={"marker": "*", "markerfacecolor": "white",
                                              "markeredgecolor": "black", "markersize": "10"}, ax=ax)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=12)  # Example rotation adjustment
    ax = sns.stripplot(x=var_x, y=pl_y, data=df, color=".1", alpha=0.4, order=orderc, ax=ax)
    ax.set_xlabel('')

    nobs = [f"n:{x:.0f}" for x in nobs.tolist()]

    # Add text to the plot
    pos = range(len(nobs))
    for tick, label in zip(pos, ax.get_xticklabels()):
        # This adds the number of samples per boxplot
        ax.text(pos[tick] / (len(nobs)) + (len(nobs) - 1) / (2 * len(nobs) ** 2) + 0.01,
                0,
                nobs[tick],
                horizontalalignment='center',
                size='x-small',
                color='b',
                weight='semibold',
                transform=ax.transAxes)
        # This shows the median on top of each boxplot
        ax.text(pos[tick] / (len(nobs)) + (len(nobs) - 1) / (2 * len(nobs) ** 2) + 0.01,
                0.975,
                round(desc["median"].values[tick], 2),
                horizontalalignment='center',
                size='x-small',
                color='gray',
                weight='semibold',
                transform=ax.transAxes)
    return ax


if getattr(sys, 'frozen', False):
    # The application is frozen
    folder_path = os.path.dirname(sys.executable)
else:
    # The application is not frozen
    folder_path = os.path.dirname(os.path.abspath(__file__))

# Append a backslash if you need it for further path building
folder_path += "\\"

# folder_path = "C:\\Users\\HYDN02\\Seafile\\Code\\DataExamples\\Florian_jv\\new_set\\"
print(folder_path)
folders = glob(folder_path + "*\\") + [folder_path]

chars = pd.DataFrame()
curves = pd.DataFrame()
mppc = pd.DataFrame()

for c, folder in enumerate(folders):
    char, curve, mpp = load_files(folder)

    if c == 0:
        chars = char.copy()
        curves = curve.copy()
        mppc = mpp.copy()
    else:
        chars = pd.concat([chars, char]).reset_index(drop=True)
        curves = pd.concat([curves, curve], axis=1).reindex(curves.index)
        mppc = pd.concat([mppc, mpp], axis=1).reindex(mppc.index)

save_folder = folder_path + "Results\\"

try:
    os.mkdir(save_folder)
except FileExistsError:
    pass

chars.to_excel(save_folder + "jv_data.xlsx")
plot_curves_together(chars, curves, save_folder)
plot_all_max_pce_together(chars, curves, save_folder)
plot_mpp(mppc, save_folder)
box_plot_making_main(folder_path)
box_plot_making_resistance(folder_path)

for sub_name in chars["subfolder"].unique():
    dataset = chars.loc[chars['subfolder'] == sub_name]
    sample_names = dataset['SampleName'].tolist()
    filtered_curves = curves.filter(regex='|'.join(sample_names))

    plot_curves_together(dataset, filtered_curves, save_folder, sub_name)
    plot_4x5_samples(filtered_curves, sub_name, save_folder)
    # plot_mpp(datampp, save_folder, sub_name)

print("Finished")
